# 
# ytdown3 - youtube video/playlist downloader (via python3)
#
# Version 1.1 as of 2024-04-13
# Author: malyg0s
#
from pytube import Playlist,YouTube     #pip install pytube
import os
import sys
import subprocess           #subprocess
import pathlib              #pathlib.Path().stem
import random               #random.random


def fatal_error(msg):
    print( "")
    print( "Fatal ERROR: ", msg)
    print( "")
    exit(1)

def print_usage():
    print( "Usage: ytdown3.py [ OPTIONS ] MODE PATH URL" )
    print( "")
    print( "where  MODE := oneshot: download only one video")
    print( "               playlist: download entire playlist")
    print( "")
    print( "       PATH := directory where to save downloaded videos (e.g. 'top_music')")
    print( "               ytdown3.py will attempt to create the directory")
    print( "")
    print( "       URL := https://www.youtube.com/playlist?list=....")
    print( "              https://www.youtube.com/watch?v=....")
    print( "")
    print( "       OPTIONS: -h (help) | -v (verbose)")
    print( "               --res=high: download in high resolution (default)")
    print( "               --res=low:  download in low resolution")
    print( "               --convert: also convert downloaded videos to .ogg audio {ffmpeg}")
    print( "")
    print( "               --audio:      equivalent with [--res=low --convert]")
    print( "               --enumerate:  prepend each filename with its number in playlist")
    print( "")
    print( "               --nodownload: skip downloading (third argument is ignored but required)")



def yt_video_download( video, msg='', 
                      verbose=False,
                      filename_prefix='',
#                      subtitles=False, subtitles_only=False,
                      get_highest_resolution=False
    ):
    print( "Downloading:", "("+video.title+")", msg, flush=True)

    vid=None
    if get_highest_resolution:
        vid = video.streams.get_highest_resolution()
    else:
        #Gets the one with lowest resolution (as of 3.3.2024)
        vid = video.streams.first()

    if verbose:
        print("--> Metadata:", vid)

#    if not subtitles_only:
    vid.download(SAVEPATH, filename_prefix=filename_prefix)

#    if subtitles:
#        all_captions = video.captions.all( )
#        for c in all_captions:
#            print( c.download(title="a") )
#        print(all_captions)

    print( "--> Filesize:", round(vid.filesize/(1024**2),2), "MiB", flush=True)








#
# MAIN
#
if __name__ == "__main__":
    verbose=False
    low_res=False
    convert=False
#    subtitles=False
#    subtitles_only=False
    nodownload=False
    onlyone=False
    enumerate_playlist=False


    #
    # READ CMDLINE ARGUMENTS
    #
    if ('-h' in sys.argv) or ('--help' in sys.argv):
        print_usage()
        exit(0)

    if '-v' in sys.argv:
        verbose=True
        sys.argv.remove('-v')



    if '--nodownload' in sys.argv:
        nodownload=True
        sys.argv.remove('--nodownload')

    if '--enumerate' in sys.argv:
        enumerate_playlist=True
        sys.argv.remove('--enumerate')

    if '--audio' in sys.argv:
        low_res=True
        convert=True
        sys.argv.remove('--audio')


#    if '--subtitles_only' in sys.argv:
#        subtitles_only=True
#        subtitles=True
#        sys.argv.remove('--subtitles_only')

#    if '--subtitles' in sys.argv:
#        subtitles=True
#        sys.argv.remove('--subtitles')

    if '--convert' in sys.argv:
        convert=True
        sys.argv.remove('--convert')

    if '--res=low' in sys.argv:
        low_res=True
        sys.argv.remove('--res=low')
        if nodownload:
            print_usage()
            fatal_error("conflicting options!")

    if '--res=high' in sys.argv:
        sys.argv.remove('--res=high')
        if low_res or nodownload:
            print_usage()
            fatal_error("conflicting options!")


    #
    # The main arguments check & load
    #
    if len(sys.argv) != 4:
        print_usage()
        fatal_error("invalid options")

    mode = sys.argv[1]
    match mode:
        case 'oneshot':
            onlyone=True
        case 'playlist':
            pass #default
        case _:
            print_usage();
            fatal_error("invalid mode")

    new_folder_name = sys.argv[2]
    yt_url = sys.argv[3]






    #
    # CREATE OUTPUT DIR
    #
    try:
        os.mkdir(new_folder_name)
        print('Created empty folder', new_folder_name)
    except:
        print('Folder already exists')
        if nodownload:
            pass
        else:
            #{
            #0:  = `first parameter`
            ##   = use "0x" prefix
            #0   = fill with zeroes
            #{1} = to a length of `second parameter` characters (including 0x)
            #x   = hexadecimal number, using lowercase letters for a-f
            #}
            suffix=("{0:#0{1}X}".format(int(random.random() * 16**8),10))[2:]
            new_folder_name = new_folder_name+"_"+suffix
            try:
                os.mkdir(new_folder_name)
                print('Created empty folder', new_folder_name)
            except:
                print('IO ERROR')
                fatal_error("expecting the target directory to be nonexistent")

    os.chdir(new_folder_name)
    SAVEPATH = os.getcwd()






    #
    # Download
    #
    if not nodownload:
        if not onlyone:
            p = Playlist(yt_url)
            print( "List size is:", len(p.video_urls) )
    
            counter = 1
            prefix=''
            for video in p.videos:

                if enumerate_playlist:
                    prefix = f'{counter} - '
                
                yt_video_download(
                    video, msg=str(counter) + " / " + str(len(p.video_urls)),
                    verbose=verbose,
                    filename_prefix=prefix,
#                    subtitles=subtitles,
#                    subtitles_only=subtitles_only,
                    get_highest_resolution=(not low_res)
                    )
                counter -=- 1

        else: #onlyone
            video = YouTube(yt_url)
            yt_video_download(
                    video,
                    verbose=verbose,
#                    subtitles=subtitles,
#                    subtitles_only=subtitles_only,
                    get_highest_resolution=(not low_res)
            )

        #
        # Fin playlist download
        #
        print( "Download finished" )
        print( "")





    #
    # Start conversion of ALL FILES IN FOLDER
    #
    if convert:
        counter = 1
        NEW128 = []
        FAILED = []
        directory_listing = os.listdir();
        for file in directory_listing:
            if not os.path.isdir(file):
                print( "Converting:", "("+file+")",
                      counter, "/", len(directory_listing), flush=True )

                newname = pathlib.Path(file).stem + ".ogg"
                try:
                    subprocess.run(
                            ["ffmpeg","-i",file,
                             "-vn","-b:a","256k","-acodec","libvorbis",
                             newname], 
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.STDOUT,
                            check=True
                    )
                    print( "--> Converted:", "("+newname+")", flush=True )

                except subprocess.CalledProcessError:
                    os.remove(newname)
                    try:
                        subprocess.run(
                            ["ffmpeg","-i",file,
                             "-vn","-b:a","128k","-acodec","libvorbis",
                             newname], 
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.STDOUT,
                            check=True
                        )
                        NEW128.append(newname)
                        print( "--> Converted 128k:", "("+newname+")", flush=True )

                    except subprocess.CalledProcessError:
                        FAILED.append(file)
                        print( "--> CONVERSION FALIED", flush=True )


            else:
                print( "Skipping directory:", "("+file+")", flush=True )

            counter -=- 1

        #
        # Fin convert
        #
        print( "Convert finished" )
        print( "")
        if len(NEW128) > 0:
            print( "The following files have 128k bitrate instead of 256k", NEW128,
                  sep='\n')
        if len(FAILED) > 0:
            print( "The following conversions have FAILED", FAILED )




    #
    # Fin - Convert
    #
    print("Have a fun day, BYE!")
    print("")
