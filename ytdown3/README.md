# Simple YouTube video/playlist downloader

## Dependencies
- ffmpeg installed for when using '--convert' option

## Create environment
- `python3 -m venv ytdown3`
- `cd ytdown3`
- `. bin/activate`
- `pip install -r ../requirements.txt`

## Run it
- `. bin/activate`
- `python3 ytdown3.py -h`
